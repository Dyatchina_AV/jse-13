package ru.dyatchina.tm;

import ru.dyatchina.tm.controller.*;
import ru.dyatchina.tm.repository.ProjectRepository;
import ru.dyatchina.tm.repository.TaskRepository;
import ru.dyatchina.tm.repository.UserRepository;
import ru.dyatchina.tm.service.*;

import java.util.Scanner;

import static ru.dyatchina.tm.constant.TerminalConst.*;

/*
*Учебное приложение "Таск-менеджер"
*
*/


public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);
    private final AuthService authService = new AuthService(userRepository);
    private final UserService userService = new UserService(userRepository);
    private final TaskService taskService = new TaskService(taskRepository, authService);
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository, userRepository, authService);



    private final ProjectController projectController = new ProjectController(projectService);
    private final TaskController taskController = new TaskController(taskService, projectTaskService);
    private final UserController userController = new UserController(userService);
    private final AuthController authController = new AuthController(authService);
    private final SystemController systemController = new SystemController();



    {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");
    }

    public static void main(final String[] args) {
         final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args){
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
        }


    public int run(final String param){
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();



            default: return systemController.displayError();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
