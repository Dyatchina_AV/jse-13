package ru.dyatchina.tm.controller;

import ru.dyatchina.tm.entity.Role;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.service.UserService;

import java.util.List;

import static ru.dyatchina.tm.constant.TerminalConst.*;

public class UserController extends  ABSController{
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void printAbstractCommands() {
        System.out.println(USER_CREATE + " <login> <password> <role> - create new User");
        System.out.println(USER_LIST + "list all User");
        System.out.println(USER_CLEAR + "clear all User");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {

        switch (commandWithArgs[0]) {
            case USER_CREATE:
                createUser(commandWithArgs);
                return true;
            case USER_LIST:
                printUsers(userService.getAllUsers());
                return true;
            case USER_CLEAR:
                userService.deleteAllUsers();
                return true;
            default:
                return false;
        }
    }

    private void printUsers(List<User> users) {
        String[][] printData = new String[users.size() + 1][3];
        printData[0][0] = " ID ";
        printData[0][1] = " Login ";
        printData[0][2] = " Role ";

        int row = 1;
        for (User user : users) {
            printData[row][0] = String.valueOf(user.getId());
            printData[row][1] = String.valueOf(user.getLogin());
            printData[row][2] = String.valueOf(user.getRole());
            row++;

            printDataWithHeader(printData);
        }
    }

    private void createUser(String[] commandWithArgs) {
        if (commandWithArgs.length < 4) {
            System.out.println("Error! No argument!");
            return;
        }
        try {
            String login = commandWithArgs[1];
            String password = commandWithArgs[2];
            Role role = Role.valueOf(commandWithArgs[3]);
            boolean success = userService.createUser(login, password, role);
            if (success) {
                System.out.println("User created");
            }
            else {
                System.out.println("User not created");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error! Invalid argument!");
        }
    }


}
