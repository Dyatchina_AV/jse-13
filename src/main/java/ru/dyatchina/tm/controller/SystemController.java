package ru.dyatchina.tm.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static ru.dyatchina.tm.constant.TerminalConst.*;

public class SystemController extends ABSController{
    public static final int MAX_HISTORY_SIZE = 10;
    private final Queue<String> history = new LinkedList<>();

    @Override
    public void printAbstractCommands() {

    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        history.add(String.join("", commandWithArgs));
        if (history.size() > MAX_HISTORY_SIZE) {
            history.poll();
        }
        switch (commandWithArgs[0])
        {
            case VERSION:
                displayVersion();
                return true;
            case ABOUT:
                displayAbout();
                return true;
            case HELP:
                displayHelp();
                return true;
            case EXIT:
                displayExit();
                return true;
            case SHOW_HISTORY:
                showHistory();
                return true;
        }
        return false;
    }

    private void showHistory() {
        System.out.println(history);
    }


    public int displayExit() {
        System.out.println("Terminal program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** Welcome to task manager **");
    }

    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info");
        System.out.println("help - Display list of commands");
        System.out.println("exit - Terminate console application");
        System.out.println("show-history - Show all histiry commands");
        System.out.println();
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-view - View project by index.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println();
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-view - View task by index.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println("task-set-user - <task id> <user id> set Task to User");
        System.out.println();
        System.out.println("user-create -  <login> <password> <role> - create new User.");
        System.out.println("user_list - list all User.");
        System.out.println("user_clear - clear all User.");
        System.out.println();
        return 0;
    }

    public int displayAbout() {
        System.out.println("Anastasiya Dyatchina ");
        System.out.println("thenochnaya@mail.ru");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }


}
