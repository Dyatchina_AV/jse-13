package ru.dyatchina.tm.controller;


import ru.dyatchina.tm.entity.Task;
import ru.dyatchina.tm.service.ProjectTaskService;
import ru.dyatchina.tm.service.TaskService;

import java.lang.ref.PhantomReference;
import java.util.List;

import static ru.dyatchina.tm.constant.TerminalConst.*;

public class TaskController extends ABSController{

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void printAbstractCommands() {
        System.out.println(TASK_CREATE + " <name> <description> - create new Task");
        System.out.println(TASK_LIST + " list all Tasks");
        System.out.println(TASK_CLEAR + " clear all Tasks");
        System.out.println(TASK_SET_USER + " <task id> <user id> assign Task to User");
        System.out.println(DELETE_TASK_SET_USER + " <task id> <user id> delete Task to User");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        switch (commandWithArgs[0]) {
            case TASK_CLEAR:
                clearTask();
                return true;
            case TASK_CREATE:
                createTask();
                return true;
            case TASK_LIST:
                listTask();
                return true;
            case TASK_VIEW:
                viewTaskByIndex();
                return true;
            case TASK_REMOVE_BY_NAME:
                removeTaskByName();
                return true;
            case TASK_REMOVE_BY_ID:
                removeTaskById();
                return true;
            case TASK_REMOVE_BY_INDEX:
                removeTaskByIndex();
                return true;
            case TASK_UPDATE_BY_INDEX:
                updateTaskByIndex();
                return true;
            case TASK_ADD_TO_PROJECT_BY_IDS:
                addTaskToProjectById();
                return true;
            case TASK_LIST_BY_PROJECT_ID:
                listTaskByProjectId();
                return true;
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                removeTaskFromProjectById();
                return true;
            case TASK_SET_USER:
                assignTaskToUser(commandWithArgs);
                return true;
            case DELETE_TASK_SET_USER:
                deleteTaskToUser(commandWithArgs);
                return true;

            default:
                return false;
        }
    }

    private void deleteTaskToUser(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long taskId = Long.parseLong(commandWithArgs[1]);
            Long userId = Long.parseLong(commandWithArgs[2]);
            boolean success = projectTaskService.deleteTaskToUser(taskId, userId);
            if (success) {
                System.out.println("Task delete to User");
            }
            else {
                System.out.println("Task not delete to User");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument!");
        }
    }

    private void assignTaskToUser(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long taskId = Long.parseLong(commandWithArgs[1]);
            Long userId = Long.parseLong(commandWithArgs[2]);
            boolean success = projectTaskService.assignTaskToUser(taskId, userId);
            if (success) {
                System.out.println("Task assigned to User");
            }
            else {
                System.out.println("Task not assigned to User");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument!");
        }
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        taskService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("PLEASE, ENTER TASK INDEX: ");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER PROJECT NAME]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION]");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        int index = 1;
        viewTasks(taskService.findAll());
        System.out.println();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("PLEASE, ENTER TASK INDEX: ");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;

    }

    public int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }


    public int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID: ");
        final Long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX: ");
        final int index = scanner.nextInt();
        final Task task = taskService.removeByIndex(index);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {

        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }

    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASK BY PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final Long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectById() {
        System.out.println("[ADD TASK TO PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID: ");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectById() {
        System.out.println("[REMOVE TASK TO PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID: ");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }


}
