package ru.dyatchina.tm.controller;

import java.util.Scanner;

public abstract class ABSController {
    protected final Scanner scanner = new Scanner(System.in);

    public abstract void printAbstractCommands();

    public abstract boolean execute(String[] commandWithArgs);

    protected void printDataWithHeader(String[][] data) {
        int[] columnsMaxWith = new int[data[0].length];
        for (int row = 0; row < data.length; row++) {
            for (int column = 0; column < data[0].length; column++){
                columnsMaxWith[column] = Integer.max(columnsMaxWith[column], data[row][column].length());
            }
        }

        for (int row = 0; row < data.length; row++) {
            for (int column = 0; column < data[0].length; column++) {
                System.out.print("|");
                System.out.print(data[row][column]);
                printSpaces (columnsMaxWith[column] - data[row][column].length());
            }

            System.out.print("|");
            System.out.println();
        }
    }

    protected void printSpaces(int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(" ");
        }
    }
}
