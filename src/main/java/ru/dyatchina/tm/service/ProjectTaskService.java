package ru.dyatchina.tm.service;

import ru.dyatchina.tm.entity.Project;
import ru.dyatchina.tm.entity.Task;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.repository.ProjectRepository;
import ru.dyatchina.tm.repository.TaskRepository;
import ru.dyatchina.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final AuthService authService;

        public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository, UserRepository userRepository, AuthService authService) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.authService = authService;
    }

    public boolean assignTaskToUser(Long taskId, Long userId) {
        if (authService.getCurrentUser() == null) {
            return false;
        }

        Task task = taskRepository.findById(taskId);
        User user = userRepository.findById(userId);
        if (task == null || user == null) {
            return false;
        }
        task.setUserId(userId);
        taskRepository.save(task);
        return true;
    }

    public boolean deleteTaskToUser(Long taskId, Long userId) {
        if (authService.getCurrentUser() == null) {
            return false;
        }

        Task task = taskRepository.findById(taskId);
        User user = userRepository.findById(userId);
        if (task == null) {
            return false;
        }
        userId = null;
        taskRepository.save(task);
        return true;
    }

    public List<Task> findAddByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAddByProjectId(projectId);
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }
}
